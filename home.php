<?php
	include 'config.php';

	$width 			= 16;
	$heigth			= 12;
	$row			= 1;

	for($row=0; $row < $heigth; $row++) { 
		for($col=0; $col < $width; $col++) { 
			$crossword[$row][$col]	= '' ;
		}
	}

	die();

	$word_num 		= 0;
	$empty_spaces	= 1;
	$position 		= array(
		rand(0, $heigth),
		rand(0, $width),
	);

	while($empty_spaces>0) {
		// 0 - vertical, 1 - horizontal
		$direction 		= rand(0,1);
		$space_ok 		= false;

		$word_num++;
		
		while(!$space_ok) {
			$letter_qtt		= array_rand($words);
			$way_letters 	= array();
			$error 			= 0;

			for($i=0; $i <= $letter_qtt; $i++) { 
				if($direction) {
					if(!isset($crossword[$position[0]][$position[1] + $i]) || is_numeric($crossword[$position[0]][$position[1] + $i])) {
						$error++;
						break;
					}else{
						$way_letters[$i] 	= $crossword[$position[0]][$position[1] + $i];
					}
				}else{
					if(!isset($crossword[$position[0] + $i][$position[1]]) || is_numeric($crossword[$position[0] + $i][$position[1]])) {
						$error++;
						break;
					}else{
						$way_letters[$i] 	= $crossword[$position[0] + $i][$position[1]];
					}
				}
			}

			if($error>0) {
				if($i>3) {
					$letter_qtt 	= $i - 1;
					$space_ok		= true;
				}else{
					$direction 		= ($direction==1) ? 0 : 1;
				}
			}else{
				$space_ok	= true;
			}
		}

		$word_ok 		= false;

		while(!$word_ok) {
			$word		= $words[$letter_qtt][array_rand($words[$letter_qtt])]['word'];
			$error 		= 0;
			$first_loop = true;

			for ($i=-1; $i <= $letter_qtt; $i++) { 
				if($first_loop) {
					$first_loop 	= false;
				}else{
					if(!empty($way_letters[$i]) && $way_letters[$i]!=substr($word, $i, 1)) {
						$error++;
						break;
					}
				}
			}

			if($error==0) {
				$word_ok 	= true;
			}
		}


		$first_loop 	= true;

		for($i=-1; $i < $letter_qtt; $i++) { 
			if($first_loop) {
				$crossword[$position[0]][$position[1]] 	= $word_num;
				$first_loop 							= false;
			}else{
				if($direction) {
					$crossword[$position[0]][$position[1] + ($i + 1)] 	= substr($word, $i, 1);
				}else{
					$crossword[$position[0] + ($i + 1)][$position[1]] 	= substr($word, $i, 1);
				}
			}
		}

		$empty_spaces 	= 0;
		$first_loop 	= true;
		foreach($crossword as $row => $cols) {
			foreach($cols as $col => $value) {
				if(empty($value)) {
					$empty_spaces++;

					if($first_loop) {
						$position 	= array(
							$row,
							$col
						);

						$first_loop = false;
					}
				}
			}
		}

		if($empty_spaces<=3) {
			$empty_spaces = 0;
		}

		if($word_num == 6) {
			break;
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Palavras Cruzadas</title>
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600">
	<link rel="stylesheet" type="text/css" href="<?php echo URL?>assets/css/style.css">
	<script type="text/javascript">
	<?php
		// echo 'var words 	= [];';
		// foreach ($local as $key => $value) {
		// 	$n 	= explode('-', $key);
		// 	$n 	= $n[1];

		// 	echo "words[$n]= {word: '{$value['word']}', tip: '{$value['description']}'}; ";
		// }
	?>
	</script>
</head>
<body>
<div class="body-container">
	<h1>Gerador de Palavras Cruzadas</h1>
	<div class="crossword">
	<?php
		foreach($crossword as $row => $cols) {
			echo '<div class="row row-' . $row . '">';

			foreach($cols as $col => $value) {
				if(is_numeric($value)) {
					echo '<div class="column row-' . $row . '-column-' . $col . ' word-tip">' . $value . '</div>';
				}else{
					echo '<div class="column row-' . $row . '-column-' . $col . '">' . $value . '</div>';
				}
			}

			echo '</div class="row">';
		}

		// for($row=1; $row<=$heigth; $row++) {
		// 	echo '<div class="row row-' . $row . '">';
		// 	for ($col=1; $col <=$width ; $col++) { 
		// 		$index 			= str_pad($row, 4, "0", STR_PAD_LEFT) . str_pad($col, 4, "0", STR_PAD_LEFT);
		// 		$description 	= (isset($letters[$index]['description'])) ? $letters[$index]['description'] : '';

		// 		echo '<div class="', implode(' ', $letters[$index]['class']), ' column row-' . $row . '-column-' . $col . '" alt="' . $description . '" title="' . $description . '">&nbsp;</div>';
		// 	}
		// 	echo '</div class="row">';
		// }
	?>
	</div>
</div>
<div class="modal-mask"></div>
<div class="modal"></div>
<script type="text/javascript" src="<?php echo URL?>assets/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo URL?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo URL?>assets/js/mask.js"></script>
</body>
</html>