# The magnificent Crosswords generator #
System to generate infinite crosswords

This system has only the objective that the improve of my programming logics.
This is only kidding. But i'm accept suggestions

Send all to [my email](mailto: rodrigo@alves.co.de)

### PS ###
* I know. The interface it is very ugly, but i just want that system works correctly, after that i will improve the appearance.


---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------


# O magnifico gerador de palavras cruzadas #
Sistema gerador de infinitas palavras cruzadas.

Este sistema tem como objetivo o desenvolvimento da lógica de programação.
Ele não é nada mais do que uma brincadeira. Mas aceito sugestões.

Envie-as para [o meu email](mailto: rodrigo@alves.co.de)

### PS ###
* Eu sei que o layout esta precário. Estou buscando desenvolver o sistema, a aparência é algo secundário.