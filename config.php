<?php
define( 'URL', 'http://localhost/crosswords-gerador/' );

function __autoload( $class_name ) {
	$class_name = strtolower( $class_name );

	require_once "assets/php/$class_name.php";
}
