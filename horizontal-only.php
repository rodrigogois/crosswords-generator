<?php
	include 'config.php';

	$width 		= 16;
	$heigth		= 12;
	$row		= 1;

	$letters 	= array();
	$i			= 1;
	while (count($letters) < ($width * $heigth)) {
		$col_remains 	= $width;
		$col 				= 1;

		while ($col_remains>0) {
			$number 		= array_rand($words);

			if($col_remains>1) {
				if($number + 1 <= $col_remains) {
					$word_number 		= array_rand($words[$number]);
					$word 				= $words[$number][$word_number]['word'];
					$description		= $words[$number][$word_number]['description'];
					$local["word-$i"]	= $words[$number][$word_number];

					for($n = -1; $n < $number; $n++) {
						$index 						= str_pad($row, 4, "0", STR_PAD_LEFT) . str_pad($col, 4, "0", STR_PAD_LEFT);

						$letters[$index]['description'] = $description;
						$letters[$index]['letter'] 		= ($n<0) ? '&nbsp;' : substr($word, $n, 1);
						$letters[$index]['class'] 		= array(
							'word',
							"word-$i",
							'horizontal',
						);

						if($n<0) {
							$letters[$index]['class'][] 	= 'tip-column';	
						}

						$col_remains--;
						$col++;
					}

					$i++;
				}
			}else{
				$index 						= str_pad($row, 4, "0", STR_PAD_LEFT) . str_pad($col, 4, "0", STR_PAD_LEFT);
				$letters[$index]['letter'] 	= '&nbsp;';
				$letters[$index]['class'] 	= array(
					"word",
					"word-$i",
					'horizontal',
					"empty-column",
				);
				$col_remains--;
				$col++;
				$i++;
			}
		}
		$row++;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Palavras Cruzadas</title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL?>assets/css/style.css">
	<script type="text/javascript">
	<?php
		echo 'var words 	= [];';
		foreach ($local as $key => $value) {
			$n 	= explode('-', $key);
			$n 	= $n[1];

			echo "words[$n]= {word: '{$value['word']}', tip: '{$value['description']}'}; ";
		}
	?>
	</script>
</head>
<body>
<div class="body-container">
	<h1>Gerador de Palavras Cruzadas</h1>
	<div class="crossword">
	<?php
		for($row=1; $row<=$heigth; $row++) {
			echo '<div class="row row-' . $row . '">';
			for ($col=1; $col <=$width ; $col++) { 
				$index 			= str_pad($row, 4, "0", STR_PAD_LEFT) . str_pad($col, 4, "0", STR_PAD_LEFT);
				$description 	= (isset($letters[$index]['description'])) ? $letters[$index]['description'] : '';

				echo '<div class="', implode(' ', $letters[$index]['class']), ' column row-' . $row . '-column-' . $col . '" alt="' . $description . '" title="' . $description . '">&nbsp;</div>';
			}
			echo '</div class="row">';
		}
	?>
	</div>
</div>
<div class="modal-mask"></div>
<div class="modal"></div>
<script type="text/javascript" src="<?php echo URL?>assets/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo URL?>assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo URL?>assets/js/mask.js"></script>
</body>
</html>