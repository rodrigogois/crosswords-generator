<?php 
class WordDao extends DB {
	public function set( Word $word ) {

		if( is_null( $word->get_id() ) ) {
			$query = sprintf(
				'INSERT INTO cross_words ( word, description ) VALUES ( "%s", "%s" )'
				, $word->get_word()
				, $word->get_description()
			);
		} else {
			$query = sprintf(
				'UPDATE cross_words
				SET word = "%s"
					, description = "%s"
				WHERE id = %d'
				, $word->get_word()
				, $word->get_description()
				, $word->get_id()
			);
		}

		return $this->query( $query );
	}

	private function preg_seach( $pattern ) {
		$words = $this->query( sprintf(
			"SELECT id, word, description FROM cross_words WHERE UPPER( word ) REGEXP '^%s$'"
			, $pattern
		) );

		return $words ? new Word( $words[0] ) : false;
	}

	public function fill_letters( $word ) {
		if( is_string( $word ) )
			$word = str_split( $word );

		foreach( $word as &$letter )
			$letter = empty( $letter ) ? '[A-Z]' : strtoupper( $letter );

		return $this->preg_seach( implode( '', $word ) );
	}
}
?>