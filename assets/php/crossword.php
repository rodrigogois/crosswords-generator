<?php 
class CrossWord { 
	private $width;
	private $heigth;
	private $difficulty;
	private $crossword;
	private $dao;

	function __construct( $width = 16, $heigth = 12, $difficulty = 0 ) {
		$this->dao = new WordDao;

		$this->set_width( $width );
		$this->set_heigth( $heigth );
		$this->set_difficulty( $difficulty );
		
		$this->set_crossword( $this->create_vector() );
	}

	private function set_width( $width ) {
		if( !is_numeric( $width ) )
			return false;

		if( $width<0 )
			return false;

		$this->width = $width;
	}

	private function set_heigth( $heigth ) {
		if( !is_numeric( $heigth ) )
			return false;

		if( $heigth<0 )
			return false;

		$this->heigth = $heigth;
	}

	private function set_difficulty( $difficulty ) {
		if( !is_numeric( $difficulty ) )
			return false;

		if( $difficulty<0 )
			return false;

		$this->difficulty = $difficulty;
	}

	private function set_crossword( array $crossword ) {
		$this->crossword = $crossword;
	}

	private function create_vector() {
		return array_fill( 0, $this->heigth, array_fill( 0, $this->width, '') );
	}

	public function get_vector() {
		return $this->crossword;
	}
}
?>