<?php
class DB {
	private $connection;
	const USERNAME = 'root';
    const PASSWORD = '';
    const HOST = 'localhost';
    const DB = 'crosswords';

	function __construct() {
		$this->connection = $this->get_connection();
	}

	function __destruct() {
		$this->connection = null;
	}

	private function get_connection() {
		$username = self::USERNAME;
		$password = self::PASSWORD;
		$host = self::HOST;
		$db = self::DB;

		return new PDO( "mysql:dbname=$db;host=$host", $username, $password );
	}

	public function query( $command ) {
		if( !$this->connection )
			$this->connection = $this->get_connection();

		$type = explode( ' ', $command );
		$type = strtolower( $type[0] );

		switch( $type ) {
			case 'select':
				try{
					$st = $this->connection->prepare( $command );

					if( !$st->execute() )
						return false;

					return $this->trimArray( $this->onlyAssoc( $st->fetchAll() ) );
				} catch(Exception $e) {}

				break;

			default:
				try{
					$check = 0;

					$this->connection->beginTransaction();

					if( !is_array( $command ) )
						$command = [$command];

					foreach( $command as $query)
						$check = $this->connection->exec( $query );

					$this->connection->commit();

					return $check;

				}catch(Exception $e) {
					$this->connection->rollBack();
				}

				break;
		}
	}

	private function onlyAssoc( array $assoc ) {
		$count = 0;

		foreach($assoc as $key){
			for( $i=0; $i<=((count($key))/2); $i++ ){
				unset( $assoc[$count][$i] );
			}

			$count++;
		}

		return $assoc;
	}

	private function trimArray( $input ) {
		if(!is_array($input)) {
			if(is_string($input)) {
				return trim($input);
			}
		}else{
			return array_map(array($this, 'trimArray'), $input);
		}
	}
}
?>