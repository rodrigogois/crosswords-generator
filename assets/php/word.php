<?php 
class Word {
	private $id;
	private $word;
	private $description;

	function __construct( $data ) {
		$this->set_id( isset( $data['id'] ) ? $data['id'] : '' );
		$this->set_word( isset( $data['word'] ) ? $data['word'] : '' );
		$this->set_description( isset( $data['description'] ) ? $data['description'] : '' );
	}

	public function set_id( $id ) {
		if( !is_numeric( $id ) )
			return false;

		$this->id = $id;
	}

	public function set_word( $word ) {
		$this->word = $word;
	}

	public function set_description( $description ) {
		$this->description = $description;
	}

	public function get_id() {
		return $this->id;
	}

	public function get_word() {
		return $this->word;
	}

	public function get_description() {
		return $this->description;
	}

	public function _toarray() {
		return str_split( $this->word );
	}
}
?>