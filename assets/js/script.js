(function($) {
	var patt = new RegExp(/([word\-]+[0-9]+)/);

	$(document).on('ready', function() {
		$('.crossword .column').hover(function() {
			$('.crossword .column.hover').each(function() {
				$(this).removeClass('hover');
			});

			var classes 	= $(this).attr('class');

			if(patt.test(classes)) {
				var word 	= classes.match(patt)[0];

				$('.crossword .' + word).each(function() {
					$(this).addClass('hover');
				});
			}
		});

		$('.crossword .column').on('mouseleave', function() {
			$('.column.hover').each(function() {
				$(this).removeClass('hover');
			});
		});

		$('.modal-mask').on('click', function() {
			$('body').removeClass('open-modal');

			$('.modal input').each(function() {
				var word_id 	= $(this).attr('id');
				var word 		= $(this).val();
				var letters 	= word.split('');

				var i 			= n = 0;
				$('.' + word_id).each(function() {
					if(i>0) {
						$(this).html(letters[n]);
						n++;
					}
					i++;
				})
			})

			$('.modal').html('');
		});

		$('.crossword .column').on('click', function() {
			var word_id 	= $(this).attr('class').match(patt)[0];
			var number 		= word_id.split('-')[1];
			var letters 	= '';
			
			$('.' + word_id).each(function() {
				var content = $(this).html();

				if(content!='&nbsp;') {
					letters += content;
				}
			})

			$('.modal').append('<p><strong>' + words[number].tip + '</strong><br/><label for="' + word_id + '">Palavra</label> <input id="' + word_id + '" name="' + word_id + '" value="' + letters + '" maxlength="' + words[number].word.length + '" data-mask="' + 'Z'.repeat(words[number].word.length) + '"></p>');

			$('#' + word_id).mask();

			$('body').addClass('open-modal');

		});
	});
})(jQuery);

String.prototype.repeat = function(num) {
    return new Array(num + 1).join(this);
}