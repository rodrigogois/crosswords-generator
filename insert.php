<?php
include 'config.php';

$dao = new WordDao;
$words = file_get_contents( 'assets/dic.txt' );
$words = utf8_encode( $words );

$words = explode( "\n", $words );

foreach ($words as &$value) 
	$dao->set( new Word( ['id' => null, 'word' => $value, 'description' => ''] ) );
?>